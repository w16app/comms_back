import graphene
from graphene_django import DjangoObjectType
from django.db.models import Q

from .models import Location, Units, Order, OrderItems


class OrderType(DjangoObjectType):
    class Meta:
        model = Order


class FindOrder(graphene.ObjectType):
    orders = graphene.List(
        OrderType,
        search=graphene.String(),
        last=graphene.Int(),
        skip=graphene.Int(),
        order=graphene.ID(),
    )

    def resolve_orders(self, info, search=None, order=None, last=None, skip=None, **kwargs):
        qs = Order.objects.all()

        if search:
            filter = (
                Q(destination__icontains=search) |
                Q(date__contains=search) |
                Q(source__icontains=search) |
                Q(orderitems__item__contains=search)
            )
            qs = qs.filter(filter)

        if order:
            qs = qs.filter(id=order)

        if skip:
            qs = qs[skip::]

        if last:
            qs = qs.order_by('-date')[:last]

        return qs


class CreateOrder(graphene.Mutation):
    id = graphene.Int()
    date = graphene.DateTime()
    destination = graphene.Int()
    source = graphene.Int()

