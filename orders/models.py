from django.db import models
from django import template


register = template.Library()


class Location(models.Model):
    location = models.CharField(max_length=100)
    detailed_location = models.CharField(max_length=250)
    # loc = models.QuerySet(Local)

    class Meta:
        managed = True
        db_table = 'logistyka_tabela_brakow_location'


class Units(models.Model):
    unit = models.CharField(max_length=100, blank=False, null=False)

    def __unicode__(self):
        return '%s' % self.unit


class Order(models.Model):
    date = models.DateTimeField(auto_created=True, auto_now_add=True)
    destination = models.ForeignKey(Location, related_name='destination', on_delete=models.SET_NULL,
                                    null=True, blank=True)
    source = models.ForeignKey(Location, related_name='source', on_delete=models.SET_NULL, null=True, blank=True)


class OrderItems(models.Model):
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True, blank=True)
    item = models.CharField(max_length=100, null=False, blank=False)
    quantity = models.IntegerField()
    unit = models.ForeignKey(Units, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'OrderItems'
