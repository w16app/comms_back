import graphene
from graphene_django import DjangoObjectType
from django.db.models import Q
from .models import OrderItems


class ItemType(DjangoObjectType):
    class Meta:
        model = OrderItems


class Item(graphene.ObjectType):
    items = graphene.List(
        ItemType,
        item=graphene.String(),
    )

    def resolve_item(self, info, item=None, **kwargs):
        qs = OrderItems.objects.all()

        if item:
            get = (
                Q(item__icontains=item)
            )
            qs = qs.get(get)

        return qs


class FindItems(graphene.ObjectType):
    items = graphene.List(
        ItemType,
        search=graphene.String(),
        item=graphene.ID(),
    )

    def resolve_items(self, info, item=None, search=None, **kwargs):
        qs = OrderItems.objects.all()

        if search:
            filter = (
                Q(item__icontains=search)
            )
            qs = qs.filter(filter)
        if item:
            qs = qs.filter(id=item)

        return qs
